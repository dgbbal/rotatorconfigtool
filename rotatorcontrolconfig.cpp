#include <QThread>
#include <QDebug>
#include "rotatorcontrolconfig.h"
#include "ui_rotatorcontrolconfig.h"





RotatorControlConfig::RotatorControlConfig(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::RotatorControlConfig)
{
    ui->setupUi(this);

    configSerial = new ConfigRotSerial();

    populateComobo(ui->BaudrateCombo, baudStrList, true);
    populateComobo(ui->protocolCombo, protoStrList, true);
    populateComobo(ui->sensorTypeCombo, sensorStrList, true);
    populateComobo(ui->maxRotCombo, maxRotStrList, true);
    populateComobo(ui->stopCombo, rotStopStrList, true);
    populateComobo(ui->lcdCombo, lcdStrList, true);
    populateComobo(ui->displayLeadZeroCombo, displayLead0StrList, true);
    populateComobo(ui->brakeDelayCombo, brakeDelayStrList, true);
    configSerial->fillPortsInfo(ui->comportCombo);

    connect(ui->comportCombo, SIGNAL( activated(int)), this, SLOT(comportComboChanged(int)));
    connect(ui->BaudrateCombo, SIGNAL(activated(int)), this, SLOT(baudrateComboChanged(int)));
    connect(ui->protocolCombo, SIGNAL(activated(int)), this, SLOT(protocolComboChanged(int)));
    connect(ui->sensorTypeCombo, SIGNAL(activated(int)), this, SLOT(sensorTypeComboChanged(int)));
    connect(ui->maxRotCombo, SIGNAL(activated(int)), this, SLOT(maxRotComboChanged(int)));
    connect(ui->stopCombo, SIGNAL(activated(int)), this, SLOT(stopComboChanged(int)));
    connect(ui->lcdCombo, SIGNAL(activated(int)), this, SLOT(lcdComboChanged(int)));
    connect(ui->displayLeadZeroCombo, SIGNAL(activated(int)), this, SLOT(displayLeadZeroComboChanged(int)));
    connect(ui->bearingCorlineEdit, SIGNAL(editingFinished()), this, SLOT(bearingCorEditFinished()));
    connect(ui->bearingTollineEdit, SIGNAL(editingFinished()), this, SLOT(bearingTolEditFinished()));

    connect(ui->getConfigPb, SIGNAL(pressed()), this, SLOT(onGetconfigPbPressed()));

    ui->serialTextWindow->setReadOnly(true);

    connect(configSerial, SIGNAL(lineReady()), this, SLOT(onSerialLineReady()));


}

RotatorControlConfig::~RotatorControlConfig()
{
    delete ui;
}



void RotatorControlConfig::onSerialLineReady()
{

    QString inMsg = configSerial->getLineMsg();

    displayInTextWindow(inMsg);

    int startPos = inMsg.indexOf(readSOM);  // strip rubbish from start
    inMsg = inMsg.mid(startPos);

    QString cmdStr = inMsg.mid(1, 3);
    QString dataStr = inMsg.mid(4, 4);

    for(int i = 0; i < readCmdToControl.count();  i++)
    {
        if (cmdStr == readCmdToControl[i])
        {
            ((this)->*(readCmdFuncList[i])) (i, dataStr);
        }
    }

}


void RotatorControlConfig::displayInTextWindow(QString inMsg)
{

    sendToTextWindow(QString("[RX ]"), inMsg);
}

void RotatorControlConfig::displayFirmware(int /*idx*/, QString value)
{
    ui->firmwareVerTextlbl->setText(value);
}

void RotatorControlConfig::displayBaudRate(int /*idx*/, QString value)
{
    if (isFirstItemBlank(ui->BaudrateCombo))
    {
       populateComobo(ui->BaudrateCombo, baudStrList, false);
    }
    ui->BaudrateCombo->setCurrentIndex(value.toInt());
}

void RotatorControlConfig::displayProtocol(int /*idx*/, QString value)
{

    if (isFirstItemBlank(ui->protocolCombo))
    {
       populateComobo(ui->protocolCombo, protoStrList, false);
    }
    ui->protocolCombo->setCurrentIndex(value.toInt());

}

void RotatorControlConfig::displayBrakeDelay(int /*idx*/, QString value)
{
    int delay = value.toInt();
    QString delayStr = QString::number(delay);

    for (int i = 0; i < brakeDelayStrList.count(); i++)
    {
        if(delayStr == brakeDelayStrList[i])
        {
            if (isFirstItemBlank(ui->brakeDelayCombo))
            {
               populateComobo(ui->brakeDelayCombo, brakeDelayStrList, false);
            }
            ui->brakeDelayCombo->setCurrentIndex(i);
            break;
        }
    }
}

void RotatorControlConfig::displayAZ_Tol(int /*idx*/, QString value)
{
    int tol = value.toInt();    // strip the leading zero
    ui->bearingTollineEdit->setText(QString::number(tol));
}

void RotatorControlConfig::displayMaxRot(int /*idx*/, QString value)
{
    int maxRot = value.toInt();    // strip the leading zero
    QString maxRotStr = QString::number(maxRot);
    for (int i = 0; i < maxRotStrList.count(); i++)
    {
        if(maxRotStr ==  maxRotStrList[i])
        {
            if (ui->maxRotCombo->itemText(0) == " ")
            {
               populateComobo(ui->maxRotCombo, maxRotStrList, false);
            }
            ui->maxRotCombo->setCurrentIndex(i);
            break;

        }
    }
}

void RotatorControlConfig::displayRotStop(int /*idx*/, QString value)
{
    if (ui->stopCombo->itemText(0) == " ")
    {
       populateComobo(ui->stopCombo, rotStopStrList, false);
    }
    ui->stopCombo->setCurrentIndex(value.toInt());
}

void RotatorControlConfig::displayLcdDisplay(int idx, QString value)
{

}

void RotatorControlConfig::displayDisplayZero(int /*idx*/, QString value)
{

}

void RotatorControlConfig::displayDisplaySensorType(int /*idx*/, QString value)
{
    if (isFirstItemBlank(ui->sensorTypeCombo))
    {
       populateComobo(ui->sensorTypeCombo, sensorStrList, false);
    }
    ui->sensorTypeCombo->setCurrentIndex(value.toInt());
}


void RotatorControlConfig::displaySensorCorrection(int /*idx*/, QString value)
{
    int corr = value.toInt();    // strip the leading zero
    ui->bearingCorlineEdit->setText(QString::number(corr));

}

void RotatorControlConfig::displayCalibRight(int idx, QString value)
{

}

void RotatorControlConfig::displayCalibLeft(int idx, QString value)
{

}

void RotatorControlConfig::displayCurrentBearing(int idx, QString value)
{

}

void RotatorControlConfig::onGetconfigPbPressed()
{
    if (configSerial->getOpenFlag())
    {
        readFirmwareVersion();
        QThread::msleep(50);

        readBaudRate();
        QThread::msleep(50);

        readProtocol();
        QThread::msleep(50);

        readBrakeDelay();
        QThread::msleep(50);

        readSensorType();
        QThread::msleep(50);

        readRotStop();
        QThread::msleep(50);

        readMaxRot();
        QThread::msleep(50);

        readAZTolerance();
        QThread::msleep(50);

        readSensorCorrection();

    }

}



void RotatorControlConfig::populateComobo(QComboBox* comboBox, QStringList& textList, bool firstListBlank)
{
    comboBox->clear();
    if (firstListBlank)
    {
        comboBox->addItem(" ");
    }

    foreach(QString str, textList)
    {
        comboBox->addItem(str);
    }
}

bool RotatorControlConfig::isFirstItemBlank(QComboBox* comboBox)
{
    if (comboBox->itemText(0) == " ")
    {
        return true;
    }
    else
    {
        return false;
    }
}

void RotatorControlConfig::comportComboChanged(int /*index*/)
{

    configSerial->closeComport();

    if (ui->comportCombo->currentText() != comport)
    {
        comport = ui->comportCombo->currentText();
    }

    openComport(comport, baudRateIndex);

}


void RotatorControlConfig::baudrateComboChanged(int index)
{
    configSerial->closeComport();
    if(index  != baudRateIndex)
    {
        baudRateIndex = index;
    }

    openComport(comport, baudRateIndex);

}


void RotatorControlConfig::openComport(const QString comport, const int baudRateIndex)
{
    if (!comport.isEmpty() && baudRateIndex > 0 && baudRateIndex < baudStrList.count())
    {
        if (configSerial->openComport(comport, baudRateIndex - 1))
        {
            sendToTextWindow(QString("[Comport] "), QString("Serial port %1 open OK").arg(comport));

        }
        else
        {
            sendToTextWindow(QString("[Comport] "), QString("Serial port %1 failed to open").arg(comport));

        }
    }
}


void RotatorControlConfig::protocolComboChanged(int index)
{
    if (configSerial->getOpenFlag())
    {
        if (index != protocolIndex && !isFirstItemBlank(ui->protocolCombo))
        {
            protocolIndex = index;
        }

        if (protocolIndex >= 0 && protocolIndex < protoStrList.count())
        {
            sendSetCmd(setCmdToControl[setCommandSet::PROTOCOL], protocolMsgValues[protocolIndex]);
        }
    }
}


void RotatorControlConfig::sensorTypeComboChanged(int index)
{
    if (configSerial->getOpenFlag())
    {
        if (index != sensorTypeIndex && !isFirstItemBlank(ui->protocolCombo))
        {
            sensorTypeIndex = index;
        }

        if (sensorTypeIndex >= 0 && sensorTypeIndex < sensorStrList.count())
        {
            sendSetCmd(setCmdToControl[setCommandSet::SENSOR_TYPE], sensorTypeMsgValues[sensorTypeIndex]);
        }
    }
}


void RotatorControlConfig::maxRotComboChanged(int index)
{
    if (configSerial->getOpenFlag())
    {

        if (index != maxRotStopIndex && !isFirstItemBlank(ui->maxRotCombo))
        {
            maxRotStopIndex = index;
        }

        if (maxRotStopIndex >= 0 && maxRotStopIndex < maxRotStrList.count())
        {
            sendSetCmd(setCmdToControl[setCommandSet::MAXROT], maxRotMsgValues[maxRotStopIndex]);
        }
    }
}


void RotatorControlConfig::stopComboChanged(int index)
{
    if (configSerial->getOpenFlag())
    {

        if (index != stopIndex && !isFirstItemBlank(ui->stopCombo))
        {
            stopIndex = index;
        }

        if (stopIndex >= 0 && stopIndex < rotStopStrList.count())
        {
            sendSetCmd(setCmdToControl[setCommandSet::ROTSTOP], maxRotMsgValues[stopIndex]);
        }
    }
}


void RotatorControlConfig::lcdComboChanged(int index)
{

}

void RotatorControlConfig::displayLeadZeroComboChanged(int index)
{

}


void RotatorControlConfig::bearingCorEditFinished()
{

}

void RotatorControlConfig::bearingTolEditFinished()
{

}


void RotatorControlConfig::readFirmwareVersion()
{
    sendReadCmd(readCmdToControl[readCommandSet::FIRMWARE_VER]);
}

void RotatorControlConfig::readBaudRate()
{
    sendReadCmd(readCmdToControl[readCommandSet::BAUD]);
}

void RotatorControlConfig::readProtocol()
{
    sendReadCmd(readCmdToControl[readCommandSet::PROTOCOL]);
}

void RotatorControlConfig::readBrakeDelay()
{
    sendReadCmd(readCmdToControl[readCommandSet::BRAKE_DELAY]);
}

void RotatorControlConfig::readAZTolerance()
{
    sendReadCmd(readCmdToControl[readCommandSet::AZ_TOL]);
}

void RotatorControlConfig::readMaxRot()
{
    sendReadCmd(readCmdToControl[readCommandSet::MAXROT]);
}

void RotatorControlConfig::readRotStop()
{
    sendReadCmd(readCmdToControl[readCommandSet::ROTSTOP]);
}

void RotatorControlConfig::readLcdDisplay()
{
    sendReadCmd(readCmdToControl[readCommandSet::LCD_DISPLAY]);
}

void RotatorControlConfig::readDisplayZero()
{
    sendReadCmd(readCmdToControl[readCommandSet::DISPLAY_ZERO]);
}

void RotatorControlConfig::readSensorType()
{
    sendReadCmd(readCmdToControl[readCommandSet::SENSOR_TYPE]);
}



void RotatorControlConfig::readSensorCorrection()
{
    sendReadCmd(readCmdToControl[readCommandSet::SENSOR_CORRECTION]);
}

void RotatorControlConfig::readCalibRight()
{
    sendReadCmd(readCmdToControl[readCommandSet::CALIB_RIGHT]);
}


void RotatorControlConfig::readCalibLeft()
{
    sendReadCmd(readCmdToControl[readCommandSet::CALIB_LEFT]);
}

void RotatorControlConfig::readCurBearing()
{
    sendReadCmd(readCmdToControl[readCommandSet::CUR_BEARING]);
}

void RotatorControlConfig::sendReadCmd(QString cmd)
{
    QString msg;
    msg.append(readSOM);
    msg.append(cmd);
    msg.append('\r');
    configSerial->sendConfigRotMessage(msg.toLatin1());
    sendToTextWindow(QString("[TX] "), msg);

}


void RotatorControlConfig::sendSetCmd(QString cmd, QString setValue)
{
    QString msg;
    msg.append(setSOM);
    msg.append(cmd);
    msg.append(setValue);
    msg.append('\r');
    configSerial->sendConfigRotMessage(msg.toLatin1());
    sendToTextWindow(QString("[TX] "), msg);
}


void RotatorControlConfig::sendToTextWindow(QString startTxt, QString msg)
{
    QString windowMsg = startTxt + " " + msg;
    ui->serialTextWindow->appendPlainText(windowMsg.remove('\r').remove('\n').toLatin1());
}
