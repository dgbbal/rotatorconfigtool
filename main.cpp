#include "rotatorcontrolconfig.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    RotatorControlConfig w;
    w.show();
    return a.exec();
}
