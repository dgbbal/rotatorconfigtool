#ifndef ROTATORCONTROLCONFIG_H
#define ROTATORCONTROLCONFIG_H

#include <QMainWindow>
#include <QComboBox>
#include "configrotserial.h"


QT_BEGIN_NAMESPACE
namespace Ui { class RotatorControlConfig; }
QT_END_NAMESPACE



namespace readCommandSet {
    enum commandSet  {FIRMWARE_VER, BAUD, PROTOCOL, BRAKE_DELAY, AZ_TOL, MAXROT, ROTSTOP, LCD_DISPLAY, DISPLAY_ZERO, SENSOR_TYPE, SENSOR_CORRECTION, CALIB_RIGHT, CALIB_LEFT, CUR_BEARING};
}

namespace setCommandSet {
    enum commandSet  {BAUD, PROTOCOL, BRAKE_DELAY, AZ_TOL, MAXROT, ROTSTOP, LCD_DISPLAY, DISPLAY_ZERO, SENSOR_TYPE, SENSOR_CORRECTION, CALIB_RIGHT, CALIB_LEFT, SET_DEFAULTS};
}


const QChar readSOM = '#';
const QChar setSOM = '!';

class RotatorControlConfig : public QMainWindow
{
    Q_OBJECT

public:
    RotatorControlConfig(QWidget *parent = nullptr);
    ~RotatorControlConfig();

protected slots:

private slots:
    void bearingTolEditFinished();
    void bearingCorEditFinished();
    void displayLeadZeroComboChanged(int index);
    void lcdComboChanged(int index);
    void stopComboChanged(int index);
    void maxRotComboChanged(int index);
    void sensorTypeComboChanged(int index);
    void protocolComboChanged(int index);
    void baudrateComboChanged(int index);
    void comportComboChanged(int index);
    void onSerialLineReady();
    void onGetconfigPbPressed();
private:
    Ui::RotatorControlConfig *ui;
    void populateComobo(QComboBox *comboBox, QStringList &textList, bool firstListBlank);

    ConfigRotSerial* configSerial;
    QString comport;
    int baudRateIndex = 0;
    int protocolIndex = 0;
    int sensorTypeIndex = 0;
    int maxRotStopIndex = 0;
    int stopIndex = 0;

    QStringList protocolMsgValues = {"0000", "0001", "0002"};
    QStringList sensorTypeMsgValues = {"0000", "0001"};
    QStringList maxRotMsgValues = {"0000", "0001"};
    QStringList stopMsgValues = {"0000", "0001"};



    QStringList baudStrList = {"4800", "9600", "19200"};
    QStringList protoStrList = {"GS232A", "GS232B", "RotorEz"};
    QStringList sensorStrList = {"Compass", "A/D"};
    QStringList maxRotStrList = {"360", "450"};
    QStringList rotStopStrList = {"North", "South"};
    QStringList lcdStrList = {"None", "LCD"};
    QStringList displayLead0StrList = {"Yes", "No"};
    QStringList brakeDelayStrList = {"500", "1000", "1500", "2000", "2500", "3000"};


    QStringList setCmdToControl = {
                                "BDR",
                                "PRT",
                                "BDL",
                                "ATO",
                                "MRT",
                                "STP",
                                "DIS",
                                "LZO",
                                "STY",
                                "SCR",
                                "CLR",
                                "CLL",
                                "CBR",
                                "DEF"};

    QStringList readCmdToControl = {
                                    "FMW",
                                    "BDR",
                                    "PRT",
                                    "BDL",
                                    "ATO",
                                    "MRT",
                                    "STP",
                                    "DIS",
                                    "LZO",
                                    "STY",
                                    "SCR",
                                    "CLR",
                                    "CLL",
                                    "CBR"};


    enum baudrate  {BAUD_OFF, BAUD24, BAUD48, BAUD96};
    enum protocol {PROTO_OFF, GS232A, GS232B, RotorEx};
    enum sensor {SENSOR_NONE, COMPASS, AtoD};
    enum maxRotate {MAXROT_NONE, MAX360, MAX450};
    enum rotStop {STOP_NONE, NORTH, SOUTH};
    enum display {DISPLAY_NONE, LCD_YES, LCD_NO};



    void readFirmwareVersion();
    void sendReadCmd(QString cmd);
    void readBaudRate();
    void readProtocol();
    void readBrakeDelay();
    void readAZTolerance();
    void readMaxRot();
    void readRotStop();
    void readLcdDisplay();
    void readDisplayZero();
    void readSensorType();
    void readSensorCorrection();
    void readCalibRight();
    void readCalibLeft();
    void readCurBearing();

    void displayFirmware(int, QString value);
    void displayBaudRate(int idx, QString value);
    void displayProtocol(int idx, QString value);
    void displayBrakeDelay(int idx, QString value);
    void displayAZ_Tol(int idx, QString value);
    void displayMaxRot(int idx, QString value);
    void displayRotStop(int idx, QString value);
    void displayLcdDisplay(int idx, QString value);
    void displayDisplayZero(int idx, QString value);
    void displayDisplaySensorType(int idx, QString value);
    void displayCalibRight(int idx, QString value);
    void displayCalibLeft(int idx, QString value);
    void displayCurrentBearing(int idx, QString value);
    void displaySensorCorrection(int idx, QString value);

    typedef void (RotatorControlConfig:: *readCmdFunc)(int, QString);
    #define CALL_MEMBER_FUNC(object, prtToMember)
    readCmdFunc readCmdFuncList[14] = { &RotatorControlConfig::displayFirmware,
                                        &RotatorControlConfig::displayBaudRate,
                                        &RotatorControlConfig::displayProtocol,
                                        &RotatorControlConfig::displayBrakeDelay,
                                        &RotatorControlConfig::displayAZ_Tol,
                                        &RotatorControlConfig::displayMaxRot,
                                        &RotatorControlConfig::displayRotStop,
                                        &RotatorControlConfig::displayLcdDisplay,
                                        &RotatorControlConfig::displayDisplayZero,
                                        &RotatorControlConfig::displayDisplaySensorType,
                                        &RotatorControlConfig::displaySensorCorrection,
                                        &RotatorControlConfig::displayCalibRight,
                                        &RotatorControlConfig::displayCalibLeft,
                                        &RotatorControlConfig::displayCurrentBearing
                                        };



    void displayInTextWindow(QString inMsg);
    void sendToTextWindow(QString startTxt, QString msg);
    void sendSetCmd(QString cmd, QString setValue);
    void openComport(const QString comPort, const int baudRateIndex);
    bool isFirstItemBlank(QComboBox *comboBox);
};
#endif // ROTATORCONTROLCONFIG_H
