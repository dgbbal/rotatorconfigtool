#ifndef CONFIGROTSERIAL_H
#define CONFIGROTSERIAL_H

#include <QObject>
#include <QComboBox>
#include <QSerialPort>
#include <QByteArray>
#include <QSerialPortInfo>
//#include "serialcomms.h"



static const char blankString[] = QT_TRANSLATE_NOOP("SettingsDialog", "N/A");

class ConfigRotSerial : public QObject
{
    Q_OBJECT
public:
    explicit ConfigRotSerial(QObject *parent = nullptr);

    void sendConfigRotMessage(const QByteArray &msg);
    void closeComport();

    void fillPortsInfo(QComboBox* comportSel);
    bool openComport(QString comport, int baudRateIdx);
    QString error();


    bool getOpenFlag();




    QString getLineMsg();
signals:
    void lineReady();

private slots:
    void onReadyRead();
private:

    QSerialPort *sComPort = nullptr;
    QByteArray m_msg;
    QString inMsg;
    bool openFlag = false;
    QStringList comportErrMsgs = { "No Error", "Device Not Found", "Permission Error"
                                   ,"Open Error", "Parity Error", "Framing Error"
                                   ,"Break Condition", "Write Error", "Read Error"
                                   ,"Resource Error", "Unsupported Operation Error"
                                   ,"Unknown Error", "Timeout Error", "Not Open Error"
                                  };



};

#endif // CONFIGROTSERIAL_H
