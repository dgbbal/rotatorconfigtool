#include "configrotserial.h"


QSerialPort::BaudRate baudRateValue[] = {QSerialPort::Baud4800, QSerialPort::Baud9600, QSerialPort::Baud19200, QSerialPort::Baud38400};

ConfigRotSerial::ConfigRotSerial(QObject *parent) : QObject(parent)
{

}


void ConfigRotSerial::sendConfigRotMessage(const QByteArray &msg)
{
    m_msg = msg;
    sComPort->write(m_msg);
}

bool ConfigRotSerial::openComport(const QString comport, int baudRateIdx)
{
    sComPort = new QSerialPort;
    sComPort->setPortName(comport);
    sComPort->setBaudRate(baudRateValue[baudRateIdx]);
    connect(sComPort, SIGNAL(readyRead()), this, SLOT(onReadyRead()));

    openFlag = sComPort->open(QIODevice::ReadWrite);
    return openFlag;

}

void ConfigRotSerial::closeComport()
{

    if (openFlag)
    {
        sComPort->close();
        delete sComPort;
        openFlag = false;
    }

}

void ConfigRotSerial::onReadyRead()
{
    while (sComPort->canReadLine())
    {
            QByteArray line = sComPort->readLine();
            inMsg = QString::fromLocal8Bit(line);

            emit lineReady();

    }

}

QString ConfigRotSerial::getLineMsg()
{
    return inMsg;
}

bool ConfigRotSerial::getOpenFlag()
{
    return openFlag;
}

QString ConfigRotSerial::error()
{
    QSerialPort::SerialPortError err = sComPort->error();
    return comportErrMsgs[err];
}


void ConfigRotSerial::fillPortsInfo(QComboBox* comportSel)
{

    comportSel->clear();

    QString description;
    QString manufacturer;
    QString serialNumber;

    comportSel->addItem("");

    foreach (const QSerialPortInfo &info, QSerialPortInfo::availablePorts()) {
        QStringList list;
        description = info.description();
        manufacturer = info.manufacturer();
#if QT_VERSION > QT_VERSION_CHECK(5, 3, 0)
        serialNumber = info.serialNumber();
#endif
        list << info.portName()
             << (!description.isEmpty() ? description : blankString)
             << (!manufacturer.isEmpty() ? manufacturer : blankString)
             << (!serialNumber.isEmpty() ? serialNumber : blankString)
             << info.systemLocation()
             << (info.vendorIdentifier() ? QString::number(info.vendorIdentifier(), 16) : blankString)
             << (info.productIdentifier() ? QString::number(info.productIdentifier(), 16) : blankString);


        comportSel->addItem(list.first(), list);

    }


}
